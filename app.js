const express = require('express');
const cookieParser = require('cookie-parser');
const helmet = require("helmet");
const cors = require("cors");
const session = require("express-session");
const passport = require("passport");

const app = express();

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser((sessionUser, done) => {
    done(null, sessionUser);
});

app.use(cookieParser());
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(session({
    secret: "raifhHackSBP",
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', require('./routes/index'));

app.use( (req, res) => {
    res.sendStatus(404);
});


module.exports = app;