const passport = require('passport');
const BearerStrategy = require('passport-http-bearer');

passport.use(new BearerStrategy(
    function(token, done) {
        const user = {id:1,name:'bearer test'}
        return done(null, user, { scope: 'read' });
    }
));


module.exports = passport.authenticate('bearer', { session: false })