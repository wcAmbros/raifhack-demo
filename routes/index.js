const express = require('express');
const bearerStrategy = require('../middleware/bearerStrategy');
const router = express.Router();

router.get('/', ({user}, res) => {
  res.send({
    info:"demo API",
    available_routes:[
        "POST /api/sbp/v1/qr/register",
        "AUTH GET /api/sbp/v1/qr/{qrId}/info",
        "AUTH GET /api/sbp/v1/qr/{qrId}/payment-info",
        "AUTH POST /api/sbp/v1/refund",
        "AUTH GET /api/sbp/v1/refund/{refundId}",
    ],
    authorization:"Bearer {token}",
    docs:"https://e-commerce.raiffeisen.ru/api/doc/sbp.html#tag/qr-controller"
  });
});

/**
 * Регистрация QR-кода
 *
 * {
  "account": 40700000000000000000,
  "additionalInfo": "Доп информация",
  "amount": 1110,
  "createDate": "2019-07-22T09:14:38.107227+03:00",
  "currency": "RUB",
  "order": "1-22-333",
  "paymentDetails": "Назначение платежа",
  "qrType": "QRStatic",
  "qrExpirationDate": "2023-07-22T09:14:38.107227+03:00",
  "sbpMerchantId": "MA0000000552"
}
 * */
router.post('/api/sbp/v1/qr/register',(req, res) =>{
  res.send({
    "code": "SUCCESS",
    "qrId": "AD100004BAL7227F9BNP6KNE007J9B3K",
    "payload": "https://qr.nspk.ru/AD100004BAL7227F9BNP6KNE007J9B3K?type=02&bank=100000000007&sum=1&cur=RUB&crc=AB75",
    "qrUrl": "https://e-commerce.raiffeisen.ru/api/sbp/v1/qr/AD100004BAL7227F9BNP6KNE007J9B3K/image"
  });
});

/**
 * Получение данных по зарегистрированному ранее QR-коду
 * */
router.get('/api/sbp/v1/qr/:qrId/info',bearerStrategy, (req, res) => {
  res.send({
    "code": "SUCCESS",
    "qrId": "AD100004BAL7227F9BNP6KNE007J9B3K",
    "payload": "https://qr.nspk.ru/AD100004BAL7227F9BNP6KNE007J9B3K?type=02&bank=100000000007&sum=1&cur=RUB&crc=AB75",
    "qrUrl": "https://e-commerce.raiffeisen.ru/api/sbp/v1/qr/AD100004BAL7227F9BNP6KNE007J9B3K/image"
  });
});

/**
 * Получение информации по платежу
 * */
router.get('/api/sbp/v1/qr/:qrId/payment-info',bearerStrategy, (req, res) => {
  res.send({
    "additionalInfo": "Доп информация",
    "amount": 12399,
    "code": "SUCCESS",
    "createDate": "2020-01-31T09:14:38.107227+03:00",
    "currency": "RUB",
    "merchantId": 123,
    "order": "282a60f8-dd75-4286-bde0-af321dd081b3",
    "paymentStatus": "NO_INFO",
    "qrId": "AD100051KNSNR64I98CRUJUASC9M72QT",
    "sbpMerchantId": "MA0000000553",
    "transactionDate": "2019-07-11T17:45:13.109227+03:00",
    "transactionId": 23
  });
});

/**
 * Оформление возврата по платежу
 * {
    "amount": 150,
    "order": "test_order_007",
    "refundId": "test_refundId_007",
    "transactionId": 41
}
 * */
router.post('/api/sbp/v1/refund', bearerStrategy, (req, res)=>{
  res.send({
    "code": "SUCCESS",
    "amount": 150,
    "refundStatus": "IN_PROGRESS"
  })
})



/**
 * Получение информации по возврату
 * */
router.get('/api/sbp/v1/refund/:refundId', bearerStrategy, (req, res)=>{
  res.send({
    "code": "SUCCESS",
    "amount": 150,
    "refundStatus": "IN_PROGRESS"
  })
})


module.exports = router;
